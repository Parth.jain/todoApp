package com.example.android.floatingactionbar;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by parth on 18/1/18.
 */

public class CustomCursorAdapter extends CursorAdapter {

    public CustomCursorAdapter(Context context, Cursor c) {
        super(context, c);

    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        // when the view will be created for first time,
        // we need to tell the adapters, how each item will look
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.single_row_todo, parent, false);

        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        // here we are setting our data
        // that means, take the data from the cursor and put it in views

        TextView textViewTitle =  view.findViewById(R.id.title_text);
        textViewTitle.setText(cursor.getString(cursor.getColumnIndex(cursor.getColumnName(2))));

        TextView mDescriptionTV =  view.findViewById(R.id.description_text);
        mDescriptionTV.setText(cursor.getString(cursor.getColumnIndex(cursor.getColumnName(3))));

       // TextView mColorTV=view.findViewById(R.id.color_text);
       // mColorTV.setText(cursor.getString(cursor.getColumnIndex(cursor.getColumnName(4))));
        String color=cursor.getString(cursor.getColumnIndex(cursor.getColumnName(4)));
        if(color.equals("Red")){
            mDescriptionTV.setTextColor(Color.RED);
        }
        else if(color.equals("Green")){
            mDescriptionTV.setTextColor(Color.GREEN);
        }
        else if(color.equals("Yellow")){
            mDescriptionTV.setTextColor(Color.YELLOW);
        }
      //  notifyDataSetChanged();
    }
}
package com.example.android.floatingactionbar;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    ImageButton FAB;
    CustomCursorAdapter customAdapter = null;
    Context context = this;
    DatabaseHelper myDb;
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        myDb = new DatabaseHelper(this);
        listView = findViewById(R.id.ToDoList);
        registerForContextMenu(listView);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.e("MainActivity", "Clicked on Item" + position + " id is: " + id);
            }
        });

        FAB = findViewById(R.id.imageButton);
        FAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                floatingAdd(-1,"","","");
    }
         });
    }
    public void floatingAdd(int id, String strTitle, String strDesc, String strColor){

        final int mId=id;
        final String mTitle=strTitle;
        final String mDesc=strDesc;
        final String mColor=strColor;

        Toast.makeText(MainActivity.this, "Hello World", Toast.LENGTH_SHORT).show();
        LayoutInflater li = LayoutInflater.from(context);
        final View promptsView = li.inflate(R.layout.add_item, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);
        alertDialogBuilder.setView(promptsView);
        final EditText mEditTitle = promptsView
                .findViewById(R.id.add_title);
        final EditText mEditDescription = promptsView.findViewById(R.id.add_descrition);
        final RadioGroup rg = promptsView.findViewById(R.id.radioColor);

        if(id!=-1) {
           mEditDescription.setText(mDesc);
           mEditTitle.setText(mTitle);
           if (mColor.equalsIgnoreCase("RED")) {
               rg.check(R.id.radioRed);
           } else if (strColor.equalsIgnoreCase("GREEN")) {
               rg.check(R.id.radioGreen);
           } else if (strColor.equalsIgnoreCase("YELLOW")) {
               rg.check(R.id.radioYellow);
           }
       }
        alertDialogBuilder.setCancelable(false)
                .setPositiveButton((id!=-1)?"Update":"Add", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        int selectedRadioButtonID = rg.getCheckedRadioButtonId();

                        if (selectedRadioButtonID != -1) {

                            RadioButton selectedRadioButton = promptsView.findViewById(selectedRadioButtonID);
                            String selectedRadioButtonText = selectedRadioButton.getText().toString();
                            String title=mEditTitle.getText().toString();
                            String description=mEditDescription.getText().toString();

                            if(mId==-1) {
                                AddToDb(title, description, selectedRadioButtonText);
                            }
                            else if(mId!=-1)
                            {
                                String id=Integer.toString(mId);
                                UpdateToDb(id,title,description,selectedRadioButtonText);

                            }
                        }
                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
    @Override
    protected void onResume() {
        super.onResume();
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                customAdapter = new CustomCursorAdapter(MainActivity.this, myDb.getAllData());
                listView.setAdapter(customAdapter);
            }
        });
    }
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_context_menu, menu);
    }
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        final AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {

            case R.id.delete_id:
                deleteToDo(info.id);
                return true;

            case R.id.update_item:

                String id=Long.toString(info.id);
                getSelectedData(id);
                Toast.makeText(MainActivity.this,"Update Pressed"+id,Toast.LENGTH_LONG).show();

            default:
                return super.onContextItemSelected(item);
        }
    }
    private void deleteToDo(long id) {

        final long ID=id;
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);
        alertDialogBuilder.setTitle("Delete Item");
        alertDialogBuilder.setMessage("Are You Sure to Delete Item");
        alertDialogBuilder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String id=Long.toString(ID);
                deleteTodoItem(id);

            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void AddToDb(String title, String desc, String color) {
        boolean isInserted = myDb.insertData(title, desc, color,"0");
        if (isInserted == true) {
            CustomCursorAdapter adapter=new CustomCursorAdapter(MainActivity.this,myDb.getAllData());
            listView.setAdapter(adapter);
            Toast.makeText(MainActivity.this, "Data Inserted", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(MainActivity.this, "Data Not Inserted", Toast.LENGTH_LONG).show();
        }

    }
    public void deleteTodoItem(String id) {

        Boolean deletedRows = myDb.delete_update(id);
        if (deletedRows == true) {
            //((BaseAdapter) listView.getAdapter()).notifyDataSetChanged();
            CustomCursorAdapter adapter=new CustomCursorAdapter(MainActivity.this,myDb.getAllData());
            listView.setAdapter(adapter);

            listView.setSelection(customAdapter.getCount() - 1);
            Toast.makeText(MainActivity.this, "Data Deleted", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(MainActivity.this, "Data is not deleted", Toast.LENGTH_SHORT).show();
        }
    }
    //getTodo Data of ID for Updation
    public void getSelectedData(String id){
        String idLocal = null;
        String titleLocal=null;
        String descriptionLocal=null;
        String colorLocal=null;
        Cursor res=myDb.getIdData(id);
        if (res.getCount()==0)
        {
            Log.e("Selected Data","No data Found");
            //show message
            Toast.makeText(this, "No Data or Record Found", Toast.LENGTH_LONG).show();
            return;
        }
        else if (res.moveToFirst()){ //<--------------
            do{  //<---------if you not need the loop you can remove that
                idLocal = res.getString(res.getColumnIndex("ID"));
                titleLocal=res.getString(res.getColumnIndex("TITLE"));
                descriptionLocal=res.getString(res.getColumnIndex("DESCRIPTION"));
                colorLocal=res.getString(res.getColumnIndex("COLOR"));
                Log.d("Test","ID: "+idLocal);
            }while(res.moveToNext());
        }
        res.close();
        floatingAdd(Integer.parseInt(idLocal),titleLocal,descriptionLocal,colorLocal);
    }
    private void UpdateToDb(String Id, String title, String desc, String selRaioBtnText) {

        boolean isUpdated=myDb.updateData(Id,title,desc,selRaioBtnText);

        if(isUpdated==true)
        {
           // MainActivity.this.customAdapter.notifyDataSetChanged();

            CustomCursorAdapter adapter=new CustomCursorAdapter(MainActivity.this,myDb.getAllData());
            listView.setAdapter(adapter);
            Toast.makeText(MainActivity.this, "Data Updated", Toast.LENGTH_SHORT).show();

        }
        else{
            Toast.makeText(MainActivity.this, "Data not updated", Toast.LENGTH_SHORT).show();
        }
    }
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId())
        {
            case R.id.select_deleted_item:
                Intent intent=new Intent(getApplicationContext(),DeletedItemActivity.class);
                startActivityForResult(intent,0);
                Log.e("MainAct","Delete Activity Selected");
                return true;
            default:
                    return super.onOptionsItemSelected(item);
        }
    }
}

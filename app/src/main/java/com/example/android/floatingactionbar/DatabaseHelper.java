package com.example.android.floatingactionbar;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.CursorAdapter;

/**
 * Created by parth on 17/1/18.
 */


public class DatabaseHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "ToDo.db";
    public static final String TABLE_NAME = "todolist_table";
    public static final String COL_1 = "ID";
    public static final String COL_2 = "TITLE";
    public static final String COL_3 = "DESCRIPTION";
    public static final String COL_4 = "COLOR";
    public static final String COL_5 ="IS_DELETED";


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table IF NOT EXISTS " + TABLE_NAME + "(ID INTEGER PRIMARY KEY AUTOINCREMENT, TITLE TEXT,DESCRIPTION TEXT,COLOR TEXT,IS_DELETED TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
    }

    public boolean insertData(String todo_title, String todo_desc, String todo_color,String todo_isdeleted) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_2, todo_title);
        contentValues.put(COL_3, todo_desc);
        contentValues.put(COL_4, todo_color);
        contentValues.put(COL_5,todo_isdeleted);
        long result = db.insert(TABLE_NAME, null, contentValues);
        if (result == -1) {
            return false;
        } else {
            return true;

        }
    }
    public Cursor getAllData() {
        SQLiteDatabase db = this.getReadableDatabase();
        //Cursor res= db.rawQuery("SELECT * FROM "+TABLE_NAME,null);
        //Cursor res = db.query(TABLE_NAME, new String[]{"ID AS _id, *"}, null, null, null, null, null);
        String strQuery = "SELECT ID As _id,* FROM "+TABLE_NAME+" Where IS_DELETED=?";
        Cursor res= db.rawQuery(strQuery, new String[]{"0"});

        return res;
    }
    public Cursor getDeletedData() {
        SQLiteDatabase db = this.getReadableDatabase();
        //Cursor res= db.rawQuery("SELECT * FROM "+TABLE_NAME,null);
        //Cursor res = db.query(TABLE_NAME, new String[]{"ID AS _id, *"}, null, null, null, null, null);
        String strQuery = "SELECT ID As _id,* FROM "+TABLE_NAME+" Where IS_DELETED=?";
        Cursor res= db.rawQuery(strQuery, new String[]{"1"});

        return res;
    }
    public Cursor getIdData(String Id) {

        SQLiteDatabase db=this.getReadableDatabase();
        String strQuery = "SELECT * FROM "+TABLE_NAME+" Where ID=?";
        Cursor res= db.rawQuery(strQuery, new String[]{Id});
        return res;
    }

    public boolean delete_update(String id){
        SQLiteDatabase db=getWritableDatabase();
        ContentValues contentValues=new ContentValues();
        contentValues.put(COL_5,"1");
        db.update(TABLE_NAME,contentValues,"ID=?",new String[]{id});
        return true;
    }


    public Integer deleteData(String id) {
            SQLiteDatabase db=this.getWritableDatabase();
            return db.delete(TABLE_NAME,"ID=?",new String[]{id});

    }

    public boolean updateData(String id,String title, String description,String color){
        SQLiteDatabase db=getWritableDatabase();
        ContentValues contentValues=new ContentValues();
        contentValues.put(COL_1,id);
        contentValues.put(COL_2,title);
        contentValues.put(COL_3,description);
        contentValues.put(COL_4,color);
        db.update(TABLE_NAME,contentValues,"ID=?",new String[]{id});
        return true;
    }
    public boolean restore_update(String id){
        SQLiteDatabase db=getWritableDatabase();
        ContentValues contentValues=new ContentValues();
        contentValues.put(COL_5,"0");
        db.update(TABLE_NAME,contentValues,"ID=?",new String[]{id});
        return true;
    }


}
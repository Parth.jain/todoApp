package com.example.android.floatingactionbar;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

public class DeletedItemActivity extends AppCompatActivity {
    CustomCursorAdapter customAdapter = null;
    DatabaseHelper myDb;
    ListView listView;
    Context context=this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deleted_item);
        myDb = new DatabaseHelper(this);
        listView = findViewById(R.id.ToDoDeletedList);
        registerForContextMenu(listView);
        View empty = findViewById(R.id.empty);
        listView.setEmptyView(empty);
    }

    @Override
    protected void onResume() {
        super.onResume();
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                customAdapter = new CustomCursorAdapter(DeletedItemActivity.this, myDb.getDeletedData());
                listView.setAdapter(customAdapter);
            }
        });


    }
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.contex_menu_2, menu);
    }
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        final AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {

            case R.id.restore_item:
                restoreData(info.id);
                return true;

            case R.id.delete_permanent:
                deleteDataItem(info.id);
                Toast.makeText(DeletedItemActivity.this,"Deleted SuccessFully"+info.id,Toast.LENGTH_LONG).show();

            default:
                return super.onContextItemSelected(item);
        }
    }

    private void restoreData(long id) {
        String id_new=Long.toString(id);
        Boolean deletedRows = myDb.restore_update(id_new);
        if (deletedRows == true) {
            //((BaseAdapter) listView.getAdapter()).notifyDataSetChanged();
            //CustomCursorAdapter adapter=new CustomCursorAdapter(MainActivity.this,myDb.getAllData());
            //listView.setAdapter(adapter);

            //listView.setSelection(customAdapter.getCount() - 1);
            //Toast.makeText(MainActivity.this, "Data Deleted", Toast.LENGTH_LONG).show();
            Intent intent=new Intent(DeletedItemActivity.this,MainActivity.class);
            startActivity(intent);
        } else {
            Toast.makeText(DeletedItemActivity.this, "Data is not deleted", Toast.LENGTH_SHORT).show();
        }
    }


    private void deleteDataItem(long id) {
        final long ID=id;
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);
        alertDialogBuilder.setTitle("Delete Item");
        alertDialogBuilder.setMessage("Are You Sure to Delete Item");
        alertDialogBuilder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String id=Long.toString(ID);
                deleteTodoItem(id);

            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void deleteTodoItem(String id) {
        Integer deletedRows = myDb.deleteData(id);
        if (deletedRows>0) {
            //((BaseAdapter) listView.getAdapter()).notifyDataSetChanged();
            CustomCursorAdapter adapter=new CustomCursorAdapter(DeletedItemActivity.this,myDb.getDeletedData());
            listView.setAdapter(adapter);

            listView.setSelection(customAdapter.getCount() - 1);
            Toast.makeText(DeletedItemActivity.this, "Data Deleted", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(DeletedItemActivity.this, "Data is not deleted", Toast.LENGTH_SHORT).show();
        }
    }
}
